/*
	Mini Activity:
	Create an expressjs API designated to port 4000
	Create a new route with endpoint /hello and method GET. (Should be able to respond with "Hello World")
*/

const express = require("express");

//Mongoose is a package that allows creation of Schemas to model our data structure
//Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:123@course.yg49u.mongodb.net/B157_to-do?retryWrites=true&w=majority",{
		useNewUrlParser:true,
		useUnifiedTopology:true
});

//Set notification for connection success or failure
//Connection to the database
//Allowsus to handle errors when the initial connection is established
let db = mongoose.connection;
//if a connection error occurs, output in the console.
db.on("error", console.error.bind(console, "Connection error"));
//.bind->can be removed, bind the "error" event and the message string

//if the connection is successful, output in the console.
db.once("open", () => console.log("We're connected to the cloud database"));

//Schemas determine the structure of the documents to be written in the database.
//This acts as a blueprints to our data
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});
//Model uses schemas and are used to create/instantiate objects that corresponds to the schema
//const Task-name of variable/model, must be singular & capitalized
const Task = mongoose.model("Task", taskSchema);
//"Task"-name of collection in the db
//taskSchema(2nd parameter)-use to sepcify the schema/blueprint of the document

//in using mongoose, the package was programmed well enough that it automatically converts the singular form to plural form when creating a collection

//setup for allowing the server to handle data from requests; allows our app to read json data
app.use(express.json());

//allows our app to read data from forms
app.use(express.urlencoded({extended:true}));

/*
	Creating a new task
		1. Add a functionality to check if there are duplicate tasks
			-if the task already exists in the databse, we return an error 
			-if the task does not exist, we add it in our database.
		2. The task data will be coming from the request's body
		3. Create a Task object with a "name" field/property
*/

app.post("/tasks", (req, res) => {

	//Check if there are duplicate tasks
	//if there are no matches, the value of result is null
	Task.findOne({name: req.body.name}, (err, result) => {

		//if the document was found's name matches the info sent via the client/postman
		if (result != null && result.name == req.body.name){
			//return a message to the client/postman
			return res.send("Duplicate task found");
			//if no document was found
		} else {
			//create a new Task and save it to database
			let newTask = new Task({
				name: req.body.name
			});
			//save method will store the info to the database
			newTask.save((saveErr, savedTask) => {
				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr)
				//no error found while creating the document 
				} else {
					//returns a status code of 201 and sends a message "New Task created"
					return res.status(201).send("New Task created.")
				}
			})
		}
	})

})


	/*GET request to retrieve all the documents
	1. Retrieve all the docs
	2. if an error is encountered, print the error
	3. if no errors are found, send a success back to the client
	*/

	app.get("/tasks", (req, res) => {
		Task.find({}, (err, result) => {
			if(err) {
				console.log(err)
			} else {
				return res.status(200).json({
					data:result
				})
			}
		})
	})


/*
	Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a git repository named S30.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.

*/

const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
});

const User = mongoose.model("User", userSchema)
app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if (result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New User created.")
				}
			})
		}
	})

})


app.get('/hello', (req,res)=>{
	res.send("Hello World!")});
app.listen(port, () =>console.log(`Server is running at port ${port}`));